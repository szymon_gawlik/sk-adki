<?php
declare(strict_types = 1);
namespace Cache;
use Cache\GetCache;

interface CacheInterface
{
    public static function removeElement(string $name, array $data);

    public static function addElement(string $name, array $data);

    public static function delete(string $name);

    public static function update(string $name, int $id, array $data);

    public function checkWritingPermission(string $name);
}

final class CacheSystem extends GetCache implements CacheInterface
{
    public static function removeElement(string $name, array $data)
    {
        // TODO: Implement remove() method.
        static::checkWritingPermission($name);
        $url = self::setPath($name);
        $xml = new \DOMDocument;
        $xml->load($url);
        $content = $xml->documentElement;
        if(key($data) === 'id') {
            foreach ($content->getElementsByTagName('item') as $item) {
                $id = $item->getElementsByTagName('id')->item(0)->textContent;

                if($id == $data['id']) {
                    $item->parentNode->removeChild($item);
                    break;
                }
            }
        } else {
            foreach ($content->getElementsByTagName('item') as $item) {
                $id_period = $item->getElementsByTagName('id_period')->item(0)->textContent;
                $id_person = $item->getElementsByTagName('id_person')->item(0)->textContent;
                if ($id_period == $data[0] && $id_person == $data[1]) {
                    $item->parentNode->removeChild($item);
                    break;
                }
            }
        }
        $xml->saveXML();
        $xml->save($url);
    }

    public static function addElement(string $name, array $data)
    {
        // TODO: Implement addElement() method.
        if(gettype($data) !== 'array') {
            throw new \Error('Must be an array!');
        }

        static::checkWritingPermission($name);

        $url = self::setPath($name);
        $xml = new \DOMDocument;
        $xml->load($url);

        $item = $xml->createElement('item');
        if(key($data)) {
            while ($value = current($data)) {
                $item->appendChild($xml->createElement((string)key($data), (string)$value));
                next($data);
            }
        } else if(key($data) == 'item'){
            $item->appendChild($xml->createElement((string)$data[0]));
        }
        $xml->documentElement->appendChild($item);
        $xml->saveXML();
        $xml->save($url);
        return;
    }

    public static function delete(string $name)
    {
        // TODO: Implement delete() method.
        $url = self::setPath($name);

        if(file_exists($url)) {
            unlink($url);
        }
    }

    public static function update(string $name, int $id, array $data)
    {
        // TODO: Implement update() method.
        static::checkWritingPermission($name);
        $url = self::setPath($name);

        $content = simplexml_load_file($url);
        foreach ($content->item as $item) {
            if($item->id == $id) {
                while ($cdata = current($data)) {
                    $item->{key($data)} = $cdata;
                    next($data);
                }
            }
        }
        $content->asXml($url);
    }

    public function checkWritingPermission(string $name)
    {
        // TODO: Implement chmod() method.
        $url = self::setPath($name);

        if(!is_writable($url)) {
            throw new \Error("Nie można zapisać do pliku! Skontaktuj się z administratorem!");
        } else {
            return;
        }
    }

}