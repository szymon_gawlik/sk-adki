<?php
declare(strict_types=1);
namespace Calculations;

use Database\{Database,SelectFromDb};
use Symfony\Component\HttpFoundation\Session\Session;
use Cache\Content;
use User\User;

abstract class BalanceAbs
{
    public $totalExpenses;
    public $planExpenses;
    public $collection;
    protected $_periods;
    protected $_session;
    protected $user;

    public function __construct()
    {
        $this->user = new User();
        $this->_periods = (new SelectFromDb('periods', ['name','amount','date'], ['archive'=>0, 'subject'=>$this->user->subject]))->get();
    }
}

final class Balance extends BalanceAbs
{
    public function __construct()
    {
        $this->_session = new Session();
        parent::__construct();
    }

    // Zaległe składki
    public function getUserShortage(int $userId) : int
    {
        $paid1 = (new SelectFromDb('paid',['id_period'],['id_person'=>$userId]))->get();
        $forgive1 = (new SelectFromDb('forgive',['id_period'],['id_person'=>$userId]))->get();
        $shortage=0;

        $paid = [];
        if($paid1) {
            foreach ($paid1->item as $item) {
                $paid[] = (int)$item->id_period;
            }
        }

        $forgive = [];
        foreach((array)$forgive1->item as $item) {
            $forgive[] = (int)$item->id_period;
        }

        $periods1 = [];
        foreach($this->_periods->item as $item) {
            $periods1[] = (int)$item->id;
        }

        $unpaid = array_diff($periods1,$paid,$forgive);
        foreach($unpaid as $item) {
            $period = (new SelectFromDb('periods',['amount'],['id'=>$item],1))->getArray();
            $shortage += (int)$period[0]['amount'];
        }

        return (int)$shortage;
    }

    //Zaległe składki
    public function getTotalShortage(\stdClass $people) : int
    {
        $totalShortage = 0;

        foreach($people->item as $value) {
            $totalShortage += $this->getUserShortage((int)$value->id);
        }

        return (int)$totalShortage;
    }

    // Przychody
    public function getRevenue(\stdClass $payment) : int
    {
        $paid = (new Database())->conn->
        query("SELECT periods.amount,paid.id_period FROM periods INNER JOIN paid ON periods.id=paid.id_period");

        $availableBudget = 0;
        $payments = 0;

        if($paid->num_rows > 0) {
            while($row = $paid->fetch_assoc()) {
                $availableBudget += $row['amount'];
            }
        }

        if($payment) {
            foreach ($payment->item as $value) {
                $payments += $value->price;
            }
        }

        $this->collection = (int)$availableBudget;
        return (int)($availableBudget+$payments);
    }

    //Dostępny budżet
    public function getAvailableBudget(\stdClass $payment, \stdClass $expenses) : int
    {
        $planExpenses = (new SelectFromDb('expenses', ['price'], ['plan' => 1, 'subject' => $this->user->subject]))->get();

        $totalExpenses = 0;
        $planExpense = 0;
        if ($expenses) {
            foreach ($expenses->item as $value) {
                $totalExpenses += $value->price;
            }
        }

        if ($planExpenses) {
            foreach ($planExpenses->item as $value) {
                $planExpense += $value->price;
            }
        }

        $this->planExpenses = $planExpense;
        $this->totalExpenses = $totalExpenses;
        return ($this->getRevenue($payment) - $totalExpenses);

    }
}