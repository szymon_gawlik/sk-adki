<?php
declare(strict_types = 1);
namespace Calculations;

use Cache\CacheSystem as Cache;
use Database\Database;
use Database\UpdateDb;
use Database\SelectFromDb;
use User\User;

abstract class CheckPeriodABS
{
    public abstract static function check(int $periodID) : bool;

    public abstract function update(int $periodID) : bool;
}

final class CheckPeriod extends CheckPeriodABS
{
    public static function check(int $periodID) : bool
    {
        // TODO: Implement check() method.
        
        $paid           = Cache::getBy('paid',['id_period'=>$periodID]);
        $forgive        = Cache::getBy('forgive',['id_period'=>$periodID]);
        $users          = Cache::getObject('people');
        $countPaid      = count($paid->item);
        $countForgive   = count($forgive->item);
        $countUsers     = count($users->item);

        if($countUsers === $countPaid+$countForgive) {
            return true;
        } else return false;
    }

    public function update(int $periodID) : bool
    {
        // TODO: Implement update() method.
        $user = new User();
        if(new UpdateDb(['archive'=>1],'periods',$periodID)){
            Cache::removeElement('periods',['id'=>$periodID]);
            $periodToArchive = (new SelectFromDb('periods', ['name','amount','date'], ['id'=>$periodID]))->getArray();
            if(Cache::fileExists("archive_periods")) {
                $periods = (new SelectFromDb('periods', ['name','amount','date'], ['archive'=>1, 'subject'=>$user->subject]))->get();
                Cache::setObject($periods,'archive_periods',600);
            }
            Cache::addElement('archive_periods',$periodToArchive[0]);
            return true;
        } else return false;
    }
}