<?php

namespace Calculations;

final class CheckUserDate
{
    private $user_date = [];
    private $period_date = [];

    public function __construct($uDate, $pDate)
    {
        $this->_setDateArray($uDate,$pDate);
    }

    private function _setDateArray($uDate, $pDate)
    {
        $userDateTime = explode(' ', $uDate);
        $periodDateTime = explode(' ', $pDate);

        $this->period_date['date'] = explode('-', $periodDateTime[0]);
        $this->period_date['time']['hour'] = explode(':', $periodDateTime[1])[0];
        $this->period_date['time']['minute'] = explode(':', $periodDateTime[1])[1];
        $this->period_date['time']['second'] = explode(':', $periodDateTime[1])[2];

        $this->user_date['date'] = explode('-', $userDateTime[0]);
        $this->user_date['time']['hour'] = explode(':', $userDateTime[1])[0];
        $this->user_date['time']['minute'] = explode(':', $userDateTime[1])[1];
        $this->user_date['time']['second'] = explode(':', $userDateTime[1])[2];
    }

    public function checkUserData() : bool
    {
        $userDate = mktime($this->user_date['time']['hour'],$this->user_date['time']['minute'],$this->user_date['time']['second'],
            $this->user_date['date'][1],$this->user_date['date'][2],$this->user_date['date'][0]);
        $periodDate = mktime($this->period_date['time']['hour'],$this->period_date['time']['minute'],$this->period_date['time']['second'],
            $this->period_date['date'][1],$this->period_date['date'][2],$this->period_date['date'][0]);

        return ($userDate > $periodDate) ? true : false;
    }
}
