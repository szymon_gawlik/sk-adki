<?php
namespace Calculations;

use Database\SelectFromDb;
use Database\UpdateDb;
use DateTime;

final class UpdateExpenses
{
    private $_expenses = [];

    public function __construct()
    {
        $this->_expenses = (new SelectFromDb('expenses',['date'], ['plan'=>1, 'archive'=>0]))->getArray();
        $this->_check();
    }

    private function _check()
    {
        $date = (new DateTime())->format('d.m.Y');

        foreach ($this->_expenses as $value) {
            if($date == substr($value['date'],0,10)){
                new UpdateDb(['plan'=>0],'expenses',$value['id']);
            }
        }

    }
}