<?php
namespace Database;

class Database extends \stdClass
{
    public $conn;

    public function __construct()
    {
        $this->connection();
    }

    private function connection()
    {
        $connInfo = [
            'host' => HOST,
            'login' => USER,
            'password' => PASSWORD,
            'dbName' => NAME
        ];

        $connection = new \mysqli($connInfo['host'],$connInfo['login'], $connInfo['password'], $connInfo['dbName']);

        if($connection->connect_error) {
            throw new \Exception('Blad polaczenia z baza danych! ' .$connection->connect_error, E_USER_ERROR);
        } else {
            $this->conn = $connection;
            //$this->conn->query("TRUNCATE TABLE payment");
        }
    }

}