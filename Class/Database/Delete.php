<?php
declare(strict_types = 1);
namespace Database;

abstract class DeleteAbs extends Database
{
    abstract protected function delete() : bool;
}
class Delete extends DeleteAbs
{
    private $table;
    private $id;

    public function __construct($table, $id)
    {
        $this->table = $table;
        $this->id = $id;
        parent::__construct();
        return $this->delete();
    }

    protected function delete() : bool
    {
        // TODO: implement delete() method.
        
        $q = "DELETE FROM {$this->table} WHERE id=?";
        if($stmt = $this->conn->prepare($q)) {
            $stmt->bind_param('i', $this->id);
            $stmt->execute();
            return true;
        } else {
            return false;
        }
    }
}