<?php
namespace Database;

final class InsertToDb extends Database
{
    private $table;
    private $values = [];
    private $a_param_type= [];
    private $query;
    public $last_id;

    public function __construct()
    {
        parent::__construct();
    }

    private function query1()
    {
        $q = str_repeat('?,',count($this->values));
        $q = substr($q, 0, -1);

        $this->query = 'INSERT INTO '.$this->table. ' VALUES (null, '.$q.')';
    }

    private function paramType()
    {
        foreach($this->values as $value) {
            $type = gettype($value);
            array_push($this->a_param_type,substr($type,0,1));
        }
    }

    private function insert()
    {
        $a_params = [];
        $param_type = '';
        $n = count($this->a_param_type);
        for($i=0;$i<$n;$i++) {
            $param_type .= $this->a_param_type[$i];
        }

        $a_params[] = &$param_type;

        for($i=0;$i<$n;$i++) {
            $a_params[] = &$this->values[$i];
        }

        if($prep = $this->conn->prepare($this->query)) {
            call_user_func_array([$prep, 'bind_param'], $a_params);
            $prep->execute();
        }
    }

    public function query($query)
    {
        if($prep = $this->conn->query($query)) {
            $this->last_id = (int)$this->conn->insert_id;
            $this->conn->close();
            return true;
        } else throw new \Exception($this->conn->error);
    }
}