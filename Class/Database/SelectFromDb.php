<?php
declare(strict_types = 1);
namespace Database;

abstract class SelectAbs extends Database
{
    abstract public function get();
    abstract public function getArray() : array;
    abstract protected function _setObject(array $array);
    abstract protected function _setArray($array);
    abstract protected function _paramType();
    abstract protected function _bindParams();
    abstract protected function _setQuery();
}

class SelectFromDb extends SelectAbs
{
    private $table;
    private $fields = [];
    private $a_param_type = [];
    private $params;
    private $a_params = [];
    private $query;
    private $limit;

    public function __construct(string $table, array $fields, array $params = null, int $limit = null)
    {
        $this->fields = $fields;
        $this->table = $table;
        $this->limit = $limit;

        array_unshift($this->fields, 'id');
        parent::__construct();
            $this->params = $params;
            $this->_paramType();
            $this->_setQuery();
            $this->_bindParams();
    }

    public function get()
    {
        if($prep = $this->conn->prepare($this->query)) {
            call_user_func_array([$prep, "bind_param"],$this->a_params);
            $prep->execute();
            $meta = $prep->result_metadata();

            while ($field = $meta->fetch_field()) {
                $params[] = &$row[$field->name];
            }

            call_user_func_array([$prep, "bind_result"], $params);

            // returns a copy of a value
            $copy = create_function('$a', 'return $a;');

            $results = [];

            while ($prep->fetch()) {
                // array_map will preserve keys when done here and this way
                $results[] = array_map($copy, $params);
            }

            $this->conn->close();

            return ($this->limit === 1) ? $this->_setObject($results)->item[0] : $this->_setObject($results);

        } else throw new \Exception('Błąd w zapytaniu! ' . $this->conn->error);
    }

    public function getArray() :array
    {
        if($prep = $this->conn->prepare($this->query)) {
            call_user_func_array([$prep, "bind_param"],$this->a_params);
            $prep->execute();
            $meta = $prep->result_metadata();

            while ($field = $meta->fetch_field()) {
                $params[] = &$row[$field->name];
            }

            call_user_func_array([$prep, "bind_result"], $params);

            // returns a copy of a value
            $copy = create_function('$a', 'return $a;');

            $results = [];

            while ($prep->fetch()) {
                // array_map will preserve keys when done here and this way
                $results[] = array_map($copy, $params);
            }

            $this->conn->close();

            return $this->_setArray($results);

        } else throw new \Exception('Błąd w zapytaniu! ' . $this->conn->error);
    }

    protected function _setObject(array $array) : \stdClass
    {
        if(count($array) === 0)
            return (object) null;

        $newObject = (object) null;
        $i=0;
        foreach($array as $key => $value) {
            $tempObj = (object) null;
            foreach($this->fields as $Fkey => $Fvalue) {
                $tempObj->{$Fvalue} = $value[$Fkey];
            }
            $newObject->item[] = $tempObj;
            $i++;
        }

        return $newObject;
    }

    protected function _setArray($array)
    {
        $newArray = [];
        foreach($array as $key => $value) {
            $tempArray = [];
            foreach($this->fields as $Fkey => $Fvalue) {
                $tempArray[$Fvalue] = $value[$Fkey];
            }
            $newArray[] = $tempArray;
        }

        return $newArray;
    }

    protected function _paramType()
    {
        if($this->params !== null) {
            foreach ($this->params as $key => $value) {
                $type = gettype($value);
                array_push($this->a_param_type, substr($type, 0, 1));
            }
        }
    }

    protected function _bindParams()
    {
        $param_type = '';
        $n = count($this->a_param_type);

        for($i=0;$i<$n;$i++) {
            $param_type .= $this->a_param_type[$i];
        }

        $this->a_params[] = &$param_type;

        foreach((array)$this->params as $key => $value){
            $this->a_params[] = &$this->params[$key];
        }

    }

    protected function _setQuery()
    {
        // Tworzenie stringa z kolumnami
        $field = '';
        $params = 'WHERE ';
        foreach($this->fields as $value) {
            $field .= $value.',';
        }
        $field = substr($field, 0, -1);

        if(is_array($this->params)) {
            foreach($this->params as $column => $value) {
                $params .= $column.'=? AND ';
            }
        } else {
            $params = '';
        }

        if(is_int($this->limit)) {
            $limit = 'LIMIT '.$this->limit;
        } else {
            $limit = '';
        }

        $params = substr($params, 0, -4);

        $this->query = "SELECT $field FROM $this->table $params $limit";
    }
}