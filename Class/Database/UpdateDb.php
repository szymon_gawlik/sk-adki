<?php
declare(strist_types = 1);
namespace Database;

abstract class UpdateDbAbs extends Database
{
    protected abstract function query();
    protected abstract function paramType();
    protected abstract function update();
}

final class UpdateDb extends UpdateDbAbs
{
    private $query;
    private $fields = [];
    private $table;
    private $id;
    private $a_param_type = [];

    public function __construct(array $fields, string $table, int $id)
    {
        parent::__construct();
        $this->fields = $fields;
        $this->table = $table;
        $this->id = $id;
        $this->query();
        $this->paramType();
        $this->update();
    }

    protected function query()
    {
        $q = '';
        foreach ($this->fields as $field => $value) {
            $q .= $field . ' = ?, ';
        }
        $q = substr($q, 0, -2);
        $query = "UPDATE $this->table SET $q WHERE id = ?";

        $this->query = $query;
    }

    protected function paramType()
    {
        foreach($this->fields as $key => $value) {
            $type = gettype($value);
            array_push($this->a_param_type,substr($type,0,1));
        }
        array_push($this->a_param_type,'i');
    }

    protected function update()
    {
        $a_params = [];
        $param_type = '';
        $n = count($this->a_param_type);
        for($i=0;$i<$n;$i++) {
            $param_type .= $this->a_param_type[$i];
        }

        $a_params[] = &$param_type;

        foreach($this->fields as $key => &$value){
            $a_params[] = &$value;
        }
        $a_params[] = &$this->id;

        if($prep = $this->conn->prepare($this->query)) {
            call_user_func_array(array($prep, 'bind_param'), $a_params);
            $prep->execute();
        } else throw new \Exception('Błąd w zapytaniu! '.$this->conn->error);
    }
}
