<?php
namespace Helpers;

use Symfony\Component\HttpFoundation\RedirectResponse;
use User\User;

class Access
{
    public static function loginAccess()
    {
        $user = new User();
        if (!$user->isLogin) {
            $response = new RedirectResponse("http://{$_SERVER['SERVER_NAME']}/login.php");
            $response->send();
        }
    }
}