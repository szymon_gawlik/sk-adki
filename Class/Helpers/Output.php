<?php
namespace Helpers;

class Output
{
    public static function set($data,$error=false)
    {
        if($error) {
            $data = ["error"=>$data->getMessage()];
        }
        $data = json_encode($data);
        $data = base64_encode($data);

        return $data;
    }
}