<?php
declare(strict_types = 1);
namespace Settings;
use Symfony\Component\HttpFoundation\Session\Session;
use Database\SelectFromDb;
use Cache\CacheSystem as Cache;

abstract class AbsSettings
{
    abstract public function settings() : bool;

    abstract public function getSettings() : array;

    abstract protected function _settings() : array;
}

class Settings extends AbsSettings
{
    public function settings() : bool
    {
        // TODO: Implement settings() method.
        $session = new Session;
        if($session->get('Subject')) {
            return true;
        }else return false;

    }

    public function getSettings() : array
    {
        // TODO: Implement getSettings() method.
        if(Cache::fileExists('settings')) {
            $settings = $this->_settings();
            Cache::setArray($settings,'settings',3600*24);
        } else {
            $settings = Cache::getArray('settings');
        }
        return $settings;
    }

    protected function _settings() : array
    {
        // TODO: Implement settings() method.

        /*
         *
         * Set subject ID
         *
         */
        $session = new Session();
        $subject = $session->get('Subject');
        if(!$subject) {
            $subject = false;
        }
        $item['subject'] = $subject;

        /*
         *
         * Set subject NAME
         *
         */

        $subjectName = (new SelectFromDb('subjects',['name'],['id'=>$session->get('Subject')],1))->get();
        $item['subjectName'] = $subjectName->name;

        return $item;
    }
}