<?php
namespace User;
use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Session\Session;

class User extends UserLogin
{
    public function __construct()
    {
        $session = new Session();

        if($session->has("login")) {
            $user=(new SelectFromDb('accounts', ['name','admin','subject'], ['id'=>$session->get("id")], 1))->get();
            $this->name = $user->name;
            $this->admin = $user->admin;
            $this->subject = $user->subject;
            $this->isLogin = true;
        }
    }
}


