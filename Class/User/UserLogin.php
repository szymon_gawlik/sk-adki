<?php
namespace User;

use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Session\Session;

class UserLogin
{
    public $name;
    public $subject;
    public $admin;
    public $isLogin = false;

    public function login(int $id,String $password)
    {
        if($login=(new SelectFromDb('accounts', ['name','password','admin','subject'], ['id'=>$id], 1))->get()){
            $session = new Session();
            if(password_verify($password,$login->password)) {
                $session->set("login",true);
                $session->set("id",$id);

                $this->name = $login->name;
                $this->subject = $login->subject;
                $this->admin = $login->admin;
                $this->isLogin = true;
            } else throw new \Error("Błędne hasło!");
        } else throw new \Error("Błędne hasło!");
    }

    public function logout()
    {
        $session = new Session();
        $session->clear();
    }
}