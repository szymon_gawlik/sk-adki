<?php
require_once 'Class/MyAutoloader.php';
require_once 'vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Database\SelectFromDb;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;

$session = new Session();

if($session->get('ACP')) {
    $redirect = new RedirectResponse('admin/index.php');
    $redirect->send();
}

$request = Request::createFromGlobals();
$login = $request->request->get('login');
$pw = $request->request->get('pw');
$submit = $request->request->get('submit');

if($login != NULL && $pw != NULL) {
    $check = (new SelectFromDb('accounts', ['pw'], ['name' => $login]))->result;
    $check = password_verify($pw,$check[0]['pw']);

    if($check) {
        $session->set('ACP',1);
        $session->set('ACP_login',$login);

        $redirect = new RedirectResponse('admin/index.php');
        $redirect->send();
    } else {
        $error = 'Nieprawidłowy login lub hasło! Spróbuj jeszcze raz!';
    }
}
if($login === ''|| $pw === '') {
    $error = 'Wypełnij wszystkie pola!';
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Zaloguj się do panelu admina</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="web/js/include/bootstrap.min.js"></script>
    <script src="web/js/include/jquery-2.1.4.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="web/css/bootstrap.min.css"><link data-require="bootstrap-css@2.3.2" data-semver="2.3.2" rel="stylesheet" href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 text-center">
                <?php
                    if($error) {
                        echo "<div class='alert alert-danger h3 bolder'>{$error}</div>";
                    }
                ?>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="login">Login</label>
                        <input type="text" name="login" id="login">
                    </div>

                    <div class="form-group">
                        <label for="password">Hasło</label>
                        <input type="password" name="pw" id="password">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" name="submit">Zaloguj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
