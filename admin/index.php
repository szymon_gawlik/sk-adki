<?php include_once 'template/header.php'; ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 text-center">
            <div><a href="?t=password" class="btn btn-warning" style="width: 150px; height: 30px; margin: 15px 0">Zmień hasło</a></div>
            <div><a href="" class="btn btn-warning" style="width: 150px; height: 30px; margin: 15px 0">Archiwizacja</a></div>
            <div><a href="" class="btn btn-warning" style="width: 150px; height: 30px; margin: 15px 0">Zamknięcie roku</a></div>
            <div><a href="" class="btn btn-warning" style="width: 150px; height: 30px; margin: 15px 0">Coś</a></div>
        </div>

        <div class="col-md-9">
            <?php
                $site = $request->get('t','home');
                $sites = ['password','archive','closeYear'];

                if(in_array($site,$sites)) {
                    include_once "template/{$site}.html";
                }

            ?>
        </div>
    </div>
</div>