var app = angular.module('AdminApp', ['ngRoute','ngDialog']);

app.controller('ChangePassword', function ($scope) {
    $scope.Submit = function (form) {
        var idForm = document.getElementById(form);

        $.ajax({
            url: 'inc/' + form + '.php',
            data: new FormData(idForm),
            contentType: false,
            cache: false,
            processData: false,
            type: 'post'
        }).success(function (dataReturn) {
            returnData(dataReturn);
        });
    }
});

/*
app.directive('content', function () {
    return {
        templateUrl: function (elem, attr) {
            return 'template/'+attr.site+'.html';
        }
    }
});*/

function returnData(data) {
    var newData = angular.fromJson(data);

    if(newData.error) {
        addAlert('error',newData.error);
    } else if(newData.error === false) {
        addAlert('success',newData.message);
    }
}

function addAlert(type, message) {
    if(type == 'error') {
        return $('#alert').html('<div class="alert alert-danger"><span class="glyphicon glyphicon-remove-circle"></span> Błąd! '+ message +'</div>');
    } else if(type == 'success') {
        return $('#alert').html('<div class="alert alert-success"><span class="glyphicon glyphicon-ok-circle"></span> ' + message + '</div>');
    }
}