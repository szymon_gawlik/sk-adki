<?php
require_once '../Class/MyAutoloader.php';
require_once '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

$session = new Session();
$session->start();

if(!$session->get('ACP')) {
    $response = new RedirectResponse('../admin.php');
    $response->send();
}
$request = Request::createFromGlobals();
?>
<!DOCTYPE html>
<html lang="pl" ng-app="AdminApp">
<head>
    <title>Admin controll panel</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='http://fonts.googleapis.com/css?family=PT+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../web/css/bootstrap.min.css"><link data-require="bootstrap-css@2.3.2" data-semver="2.3.2" rel="stylesheet" href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />

    <!-- jQuery library -->
    <script src="../web/js/include/jquery-2.1.4.min.js" type="text/javascript"></script>

    <!-- Latest compiled JavaScript -->
    <script src="../web/js/include/bootstrap.min.js"></script>
    <script src="../web/js/include/angular.min.js"></script>
    <script src="../web/js/include/angular-route.min.js"></script>
    <script src="../web/js/include/ngDialog.min.js"></script>
    <script data-require="angular-ui-bootstrap@0.3.0" data-semver="0.3.0" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.3.0.min.js"></script>

    <link rel="stylesheet" href="../web/css/ngDialog.css">
    <link rel="stylesheet" href="../web/css/ngDialog-theme-default.min.css">
    <link rel="stylesheet" href="../web/css/ngDialog-theme-plain.min.css">
    <script src="js/AdminApp.js" type="text/javascript"></script>
</head>

<body>