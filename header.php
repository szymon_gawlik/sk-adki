<?php
require_once 'config.php';

use Symfony\Component\HttpFoundation\RedirectResponse;
use User\User;

$user = new User();

if(!$user->isLogin) {
    $response = new RedirectResponse('login.php');
    $response->send();
}

?>
<!DOCTYPE html>
<html lang="pl" data-ng-app="App">
<head>
    <title>Składki</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=PT+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="web/css/style.css" type="text/css" />
    <!-- Latest compiled and minified CSS -->
    <!-- jQuery library -->
    <script src="web/js/include/jquery-2.1.4.min.js" type="text/javascript"></script>

    <!-- Latest compiled JavaScript -->
    <script src="web/js/include/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular.min.js"></script>
    <script src="web/js/App.js" type="text/javascript"></script>
    <script src="web/js/include/angular-route.min.js"></script>
    <script src="web/js/include/ngDialog.min.js"></script>
    <script src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.14.0.min.js"></script>

    <link rel="stylesheet" href="web/css/ngDialog.css">
    <link rel="stylesheet" href="web/css/ngDialog-theme-default.min.css">
    <link rel="stylesheet" href="web/css/ngDialog-theme-plain.min.css">
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <base href="/">
</head>
<body data-spy="scroll" data-toggle=".navbar" data-offset="50">