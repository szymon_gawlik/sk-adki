<?php
require_once '../config.php';

use Calculations\CheckPeriod;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Request;

Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $id = (int)$request->get('id');
    $update = new CheckPeriod;
    echo $update->update($id);
} catch (Error $e) {
    echo Output::set($e,true);
}

