<?php
require_once '../config.php';

use Calculations\Balance;
use Database\SelectFromDb;
use Cache\CacheSystem as Cache;
use Helpers\Access;
use Helpers\Output;
use User\User;

Access::loginAccess();
$userC = new User();
try {
    $people = (new SelectFromDb('people',['date','name'],['display'=>1, 'subject'=>$userC->subject]))->get();

    $overdue = [];
    $balance = new Balance();
    foreach ($people->item as $user) {
        $overdue[] = ['value'=>$balance->getUserShortage((int)$user->id), 'name'=>(string)$user->name,'id'=>(int)$user->id];
    }

    $expenses = (new SelectFromDb('expenses', ['name', 'price', 'date','plan'], ['subject'=>$userC->subject]))->get();
    $payments = (new SelectFromDb('payment', ['name', 'price', 'date'], ['subject'=>$userC->subject]))->get();

    $obj                    = (object) null;
    $obj->overdue           = $overdue;
    $obj->revenue           = $balance->getRevenue($payments);
    $obj->availableBudget   = $balance->getAvailableBudget($payments,$expenses);
    $obj->totalShortage     = $balance->getTotalShortage($people);
    $obj->totalExpenses     = $balance->totalExpenses;
    $obj->planExpenses      = $balance->planExpenses;
    $obj->expenses          = $expenses;
    $obj->payments          = $payments;
    $obj->payCollect        = $balance->collection;

    echo Output::set($obj);
} catch (Error $e) {
    echo Output::set($e,true);
}