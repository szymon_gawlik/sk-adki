<?php
require_once '../config.php';

use Database\SelectFromDb;
use Calculations\Balance;
use Database\InsertToDb;
use Database\UpdateDb;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Session\Session;
Access::loginAccess();
try {
    $session = new Session();
    $session->start();
    $balance = new Balance();
    $payments = $balance->getRevenue();
    $expenses = $balance->totalExpenses;
    $year = (new DateTime('now', new DateTimeZone('Europe/Warsaw')))->format('Y')-1;
    $id_year = (new SelectFromDb('years',[],['year'=>$year]))->result;
    $users = (new SelectFromDb('people',['name'],['display'=>1]))->result;
    $last_balance = $payments-$expenses;

    if(isset($id_year)) {
        echo 'Istnieje już archiwum poprzedniego roku!';
        exit;
    }

    $u_shortage = ''; //User shortage
    $v_shortage = ''; // Value shortage

    foreach ($users as $user) {
        if($balance->getUserShortage($user['id']) > 0){
            $u_shortage .= $user['id'] . ';';
            $v_shortage .= $balance->getUserShortage($user['id']) . ';';
        }
    }

    if(new InsertToDb([$year,$payments,$expenses,$last_balance,$u_shortage,$v_shortage,$session->get('Subject')],'years')) {
        $all_expenses = (new SelectFromDb('expenses',[],['year'=>0,'archive'=>0]))->result;
        $all_payments = (new SelectFromDb('Payment',[],['year'=>0,'archive'=>0]))->result;
        $all_periods = (new SelectFromDb('periods',[],['year'=>0,'archive'=>0]))->result;
        $date = (new DateTime('now', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y H:i:s O');

        foreach ($all_expenses as $expense) {
            new UpdateDb(['year'=>$id_year[0]['id'],'archive'=>1],'expenses',$expense['id']);
        }
        foreach ($all_payments as $payment) {
            new UpdateDb(['year'=>$id_year[0]['id'],'archive'=>1],'Payment',$payment['id']);
        }
        foreach ($all_periods as $period) {
            new UpdateDb(['year'=>$id_year[0]['id'],'archive'=>1],'periods',$period['id']);
        }

        if($last_balance > 0) {
            new InsertToDb(['Saldo końcowe poprzedniego roku',$last_balance,$date,$session->get('Subject'),0,0],'Payment');
        } elseif ($last_balance < 0) {
            new InsertToDb(['Saldo końcowe poprzedniego roku',abs($last_balance),$date,0,$session->get('Subject'),0,0],'expenses');
        } else {

        }
    }

} catch (Exception $e) {
    echo Output::set($e,true);
}
