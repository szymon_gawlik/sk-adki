<?php
require_once '../config.php';

use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Request;
use Database\{UpdateDb,SelectFromDb};
Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $id = input($request->request->get('id'));

    if(empty($id)) {
        echo '{"error":"Brak argumentu!"}';
    }
    $date = (new DateTime('now'))->format('d.m.Y');
    if(new UpdateDb(['plan'=>0,'date'=>$date],'expenses',$id)) {
        $expenses = (new SelectFromDb('expenses', ['description', 'price', 'date','plan'], ['archive' => 0]))->result;
        $array = ['error'=>false, 'data' => $expenses];
        echo Output::set($array);
    }
}catch (Exception $e) {
    echo Output::set($e,true);
}