<?php
declare(strict_type = 1);
require_once '../../config.php';

use Database\SelectFromDb;
use Database\InsertToDb;
use Database\UpdateDb;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Cache\CacheSystem as Cache;
use User\User;

$user = new User();
Access::loginAccess();

try {
    $request = Request::createFromGlobals();
    $name = (string)$request->request->get('name');
    $amount = (int)$request->request->get('amount');

    if(empty($name) || empty($amount)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }

    $add = new InsertToDb();
    $add->query("INSERT INTO periods VALUES (NULL,'{$name}',{$amount},now(),{$user->subject},0)");

    $newData = (new SelectFromDb('periods', ['name','amount','date'], ['id'=>$add->last_id], 1))->getArray();
    $data = [
        'newData' => $newData,
        'error' => false
    ];
    Cache::addElement('periods',$newData[0]);

    echo Output::set($data);
} catch (Exception $e) {
    echo Output::set($e,true);
}
