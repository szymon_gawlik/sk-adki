<?php
require_once '../../config.php';

use Database\InsertToDb;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Cache\CacheSystem as Cache;
use User\User;
$user = new User();

Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $name = $request->request->get('name');
    $price = (int)$request->request->get('price');
    $inputDate = $request->request->get('date');

    if(empty($name) || empty($price) || empty($inputDate)) {
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }
    $subject = $user->subject;

    $date = explode('.', (new DateTime('now', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y'));
    $inputDateExplode = explode('-',$inputDate);
    $plan = 0;
    if(mktime(0,0,0,$inputDate[1],$inputDate[2],$inputDate[0]) > mktime(0,0,0,$inputDateExplode[2],$inputDateExplode[0],$inputDateExplode[1])) {
        $plan = 1;
    }
    $insertDate = $inputDateExplode[2].$inputDateExplode[1].$inputDateExplode[0];
    $insert = new InsertToDb();
    $insert->query("INSERT INTO expenses VALUES (null,'{$name}',{$price},CONVERT('{$inputDate}', DATE),{$plan},{$subject})");

    echo Output::set(["error"=>false]);
    //Cache::addElement('expenses',['id'=>$insert->last_id, 'name'=>$name,'price'=>$price,'date'=>$inputDate,'plan'=>$plan]);

} catch (Exception $e) {
    echo Output::set($e,true);
}