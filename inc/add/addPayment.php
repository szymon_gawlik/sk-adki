<?php
require_once '../../config.php';

use Database\InsertToDb;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Cache\CacheSystem as Cache;
use User\User;
$user = new User();

Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $name = $request->request->get('name');
    $price = (int)$request->request->get('price');
    $inputDate = $request->request->get('date');
    if(empty($name) || empty($price) || empty($request->request->get('date'))) {
        echo Output::set(["error"=>"Wypełnij wszystkie pola!"]);
        exit;
    }
    $date = explode('.', (new DateTime('now', new DateTimeZone('Europe/Warsaw')))->format('d.m.Y'));
    $plan = 0;
    if(mktime(0,0,0,$inputDate[1],$inputDate[2],$inputDate[0]) > mktime(0,0,0,$date[1],$date[0],$date[2])) {
        $plan = 1;
    }

    $subject = $user->subject;

    if($name && $price) {
        $insert = new InsertToDb();
        $insert->query("INSERT INTO payment VALUES (null,'{$name}',{$price},CONVERT('{$inputDate}',DATE),{$plan},{$subject})");
        echo Output::set(["error"=>false]);
        Cache::addElement('payments',['id'=>$insert->last_id, 'name'=>$name,'price'=>$price,'date'=>$inputDate]);

    } else {
        echo Output::set(["error"=>"Wypełnij wszystkie pola!"]);;
    }
} catch (Exception $e) {
    echo Output::set($e,true);
}