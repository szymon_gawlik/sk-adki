<?php
require_once '../../config.php';

use Database\InsertToDb;
use Database\SelectFromDb;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Calculations\CheckUserDate;
use Cache\CacheSystem as Cache;
use User\User;

Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $name = $request->request->get('name');
    if(empty($name)) {
        echo Output::set(["error"=>"Wypełnij wszystkie pola!"]);
        exit;
    }

    $subject = (new User())->subject;
    $add1 = new InsertToDb();
    $add1->query("INSERT INTO people VALUES (NULL,'{$name}',now(),{$subject},1)");

    $person = (new SelectFromDb('people', ['name','date'], ['id'=>$add1->last_id],1))->getArray();
    $person[0]['hidePaid'] = "1";
    $periods = (new SelectFromDb('periods', ['name','amount','date'], ['archive'=>0, 'subject'=>$subject]))->get();
    $display = [];

    foreach($periods->item as $period){
        if((new CheckUserDate($person[0]['date'],$period->date))->checkUserData()) {
            $add = new InsertToDb();
            $add->query("INSERT INTO forgive VALUES (NULL,{$person[0]['id']},{$period->id},now())");

            Cache::addElement('forgive',['id'=>$add->last_id,'id_period'=>$period->id,'id_person'=>$person[0]['id']]);
            Cache::addElement('display',['item'=>$period->id.$person[0]['id']]);
            $display[] = (string)$period->id.$person[0]['id'];
        }
    }

    Cache::addElement('people',['id'=>$add1->last_id, 'name'=>$name]);
    $data = [
        "error"=>false,"message"=>"Poprawnie dodano dłużnika!","display"=>$display,"person"=>$person
    ];
    echo Output::set($data);
} catch (Error $e) {
    echo Output::set($e,true);
}