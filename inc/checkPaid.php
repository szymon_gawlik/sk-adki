<?php
require_once '../config.php';

use Database\SelectFromDb;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Request;

Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $u_id = (int)$request->request->get('user_id');
    $p_id = (int)$request->request->get('period_id');

    $query = (new SelectFromDb('paid',[],['id_person'=>$u_id,'id_period'=>$p_id]))->get();

    echo ($query) ? 'true' : 'false';

} catch (Exception $e){
    echo Output::set($e,true);
}
