<?php
require_once '../config.php';

use Database\{Delete};
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\{Request,Session\Session};
use Cache\CacheSystem as Cache;
Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $id = $request->request->get('id');
    $type = $request->request->get('type');
    $types = ['periods','expense'];
    $session = new Session();
    if(in_array($type,$types)) {
        switch($type) {
            case 'periods':
                new Delete('periods', $id);
                Cache::removeElement('periods', ['id'=>(int)$id]);
                echo Output::set(["error"=>false]);
                break;

            case 'expense':
                new Delete('expenses',$id);
                Cache::removeElement('expenses',['id'=>(int)$id]);
                echo Output::set(["error"=>false]);
                break;
        }
    }

} catch (Error $e) {
    echo Output::set($e,true);
}
