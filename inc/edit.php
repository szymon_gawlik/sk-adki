<?php
require_once '../config.php';

use Database\{SelectFromDb,UpdateDb};
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\{Request,Session\Session};
use Cache\CacheSystem as Cache;
Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $id = $request->request->get('id');
    $name = $request->request->get('name');
    $amount = $request->request->get('amount');

    if(empty($name) || empty($amount)){
        echo '{"error":"Wypełnij wszystkie pola!"}';
        exit;
    }
    new UpdateDb(['name'=>$name,'amount'=>$amount],'periods',$id);
    Cache::update("periods",$id,["name"=>$name,"amount"=>$amount]);
    $periods = (new SelectFromDb('periods', ['name','amount','date'], ['id'=>$id],1))->get();


    $data = [
        'data' => $periods,
        'error' => false
    ];

    echo Output::set($data);
} catch (Exception $e) {
    echo Output::set($e,true);
}