<?php
require_once '../config.php';

use Database\UpdateDb;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Request;
use Cache\CacheSystem as Cache;
Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $name = input($request->request->get('name'));
    $price = input($request->request->get('price'));
    $date = input($request->request->get('date'));
    $id = input($request->request->get('id'));

    if(empty($name) || empty($price) || empty($date)){
        echo '{"error":"Wypełnij wszystie pola!"}';
        exit;
    }

    new UpdateDb(['name'=>$name,'price'=>$price, 'date'=>$date], 'expenses', $id);
    Cache::update("expenses",$id,['name'=>$name,'price'=>$price, 'date'=>$date]);
    echo Output::set(["error"=>false]);
}catch (Exception $e) {
    echo Output::set($e,true);
}
