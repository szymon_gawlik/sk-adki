<?php
require_once '../config.php';

use Database\UpdateDb;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Request;
use Cache\CacheSystem as Cache;
Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $id = (int)$request->request->get('id');
    if(empty($id)) {
        Output::set(["error"=>"Brak ID"]);
        exit();
    }

    if(new UpdateDb(['display'=>0],'people',$id)){
        echo Output::set(["error"=>false]);
    }

} catch (Exception $e) {
    echo Output::set($e,true);
}