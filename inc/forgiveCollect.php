<?php
require_once '../config.php';

use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Request;
use Database\{InsertToDb,SelectFromDb};
use Cache\CacheSystem as Cache;
Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $personId = (int)$request->request->get('personId');
    $periodId = (int)$request->request->get('periodId');

    if(empty($periodId) || empty($personId)) {
        echo json_encode(["error"=>"Wypełnij wszystkie pola!"]);
        exit;
    }

    $insert = new InsertToDb();
    $insert->query("INSERT INTO forgive VALUES (null,{$personId},{$periodId},NOW())");

    Cache::addElement('forgive',['id'=>$insert->last_id,'id_person'=>$personId,'id_period'=>$periodId]);
    echo Output::set(["error"=>false]);
} catch (Error $e) {
    echo Output::set($e,true);
}