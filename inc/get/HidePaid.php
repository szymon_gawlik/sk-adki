<?php
require_once '../../config.php';

use Calculations\Balance;
use Database\SelectFromDb;
use Cache\CacheSystem as Cache;
use Helpers\Access;
use Helpers\Output;
use User\User;

Access::loginAccess();
try {
    $user = new User();
    $people = (new SelectFromDb('people',['date','name'],['display'=>1, 'subject'=>$user->subject]))->get();

    $balance = new Balance;

    for($i=0;count($people->item)>$i;$i++) {
        $usersPaid = $balance->getUserShortage((int)$people->item[$i]->id);
        $people->item[$i]->hidePaid = ((int)$usersPaid == 0) ? 1 : 0;
    }

    echo Output::set($people);

} catch (Error $e) {
    echo Output::set($e,true);
}