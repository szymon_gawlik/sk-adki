<?php
declare(strict_types=1);
require_once '../../config.php';

use Database\SelectFromDb;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Request;

Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $id = (int)$request->get('id');

    $periods1 = (new SelectFromDb('periods',[]))->get();
    $paid1 = (new SelectFromDb('paid',['id_period'],['id_person'=>$id]))->get();
    $forgive1 = (new SelectFromDb('forgive',['id_period'],['id_person'=>$id]))->get();

    $paid = [];
    foreach($paid1->item as $item){
        $paid[] = $item->id_period;
    }
    $forgive = [];
    foreach($forgive1->item as $item) {
        $forgive[] = $item->id_period;
    }

    $periods = [];
    foreach($periods1->item as $item) {
        $periods[] = $item->id;
    }

    $unpaid = array_diff($periods,$paid,$forgive);
    $periodToForgive = [];

    foreach($unpaid as $item) {
        $periodToForgive[] =(new SelectFromDb('periods',['amount','name'],['id'=>$item],1))->get();
    }


    echo Output::set($periodToForgive);

} catch (Exception $e) {
    echo Output::set($e,true);
}