<?php
require_once '../../config.php';

use Calculations\Balance;
use Database\SelectFromDb;
use Cache\CacheSystem as Cache;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Session\Session;
use User\User;
$user = new User();
Access::loginAccess();
try {
    $people = (new SelectFromDb('people',['date','name'],['display'=>1, 'subject'=>$user->subject]))->get();
    $periods = (new SelectFromDb('periods', ['name','amount','date'], ['archive'=>0, 'subject'=>$user->subject]))->get();
    $paid = (new SelectFromDb('paid',['id_person','id_period']))->get();
    $forgive = (new SelectFromDb('forgive', ['id_person', 'id_period']))->get();

    $display = [];
    foreach ($forgive->item as $item) {
        $display[] = (int)$item->id_person.(int)$item->id_period;
    }

    $paid1 = [];
    foreach ($paid->item as $item) {
        $paid1[] = (int)($item->id_period.$item->id_person);
    }

    for($i=0;count($people->item)>$i;$i++) {
        $usersPaid = (new Balance)->getUserShortage((int)$people->item[$i]->id);

        $people->item[$i]->hidePaid = ($usersPaid == 0) ? 1 : 0;
    }

    $data = (object) null;

    $data->people  = $people;
    $data->periods = $periods;
    $data->display = $display;
    $data->paid    = $paid1;
    $data->forgive = $forgive;

    echo Output::set($data);
} catch (Error $e) {
    echo Output::set($e,true);
}