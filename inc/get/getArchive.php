<?php
require_once '../../config.php';

use Database\SelectFromDb;
use Cache\CacheSystem as Cache;
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Session\Session;
use User\User;

Access::loginAccess();
try {
    $user = new User();
    $people = (new SelectFromDb('people',['date','name'],['display'=>1, 'subject'=>$user->subject]))->get();
    $periods = (new SelectFromDb('periods', ['name','amount','date'], ['archive'=>1, 'subject'=>$user->subject]))->get();

    $paid = Cache::getObject('paid');
    $forgive = Cache::getObject('forgive');
    $display = Cache::getArray('display');

    $paid1 = [];
    foreach ($paid->item as $item) {
        $paid1[] = (int)($item->id_period.$item->id_person);
    }

    $data = (object) null;

    $data->people  = $people;
    $data->periods = $periods;
    $data->display = $display['item'];
    $data->paid    = $paid1;

    echo Output::set($data);
} catch (Error $e) {
    echo Output::set($e,true);
}