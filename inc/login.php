<?php
require_once '../config.php';
use Database\SelectFromDb;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Request;
use User\User;

try {
    $request = Request::createFromGlobals();
    $login = (string)$request->request->get('login');
    $password = $request->request->get('password');
    $user = new User();
    if($login && $password) {
        $login = input($login);
        $password = input($password);
        $uId = (new SelectFromDb('accounts', ['name','password','admin','subject'], ['name'=>$login], 1))->get();

        $user->login($uId->id,$password);
        echo Output::set(["error"=>false]);
    } else echo Output::set(["error"=>"Wypełnij wszystkie pola!"]);
} catch (Error $e) {
    echo Output::set($e,true);
}