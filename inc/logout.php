<?php
require_once '../config.php';

use Symfony\Component\HttpFoundation\Session\Session;

$session = new Session();

$session->clear();
