<?php
require_once '../config.php';
use Database\{SelectFromDb,Delete,InsertToDb};
use Helpers\Access;
use Helpers\Output;
use Symfony\Component\HttpFoundation\Request;
use Cache\CacheSystem as Cache;
use Calculations\CheckPeriod;
Access::loginAccess();
try {
    $request = Request::createFromGlobals();
    $userId = (int)$request->request->get('userId');
    $periodId = (int)$request->request->get('periodId');
    $archivePeriod = false;

        $selectPaid = (new SelectFromDb('paid',[],['id_person' => $userId,'id_period' => $periodId], 1))->get();

        if($selectPaid) {
            new Delete('paid',$selectPaid->id);
            Cache::removeElement('paid',[$periodId, $userId]);
            echo Output::set(["error"=>false]);
        } else {
            if((new InsertToDb())->query("INSERT INTO paid VALUES (null,{$userId},{$periodId},now())")){
                Cache::addElement('paid',['id_period'=>$periodId,'id_person'=>$userId]);
            }

            if(CheckPeriod::check($periodId)) {
                $archivePeriod = true;
            }
            echo Output::set(["error"=>false, "data"=>"add", "archivePeriod"=>$archivePeriod]);
        }

} catch (Error $e) {
    echo Output::set($e,true);
}
