<?php include_once 'header.php'; ?>
<!--- BEGIN MENU --->
<nav class="navbar navbar-default navbar-fixed-top">
    <?php include_once 'web/template/main_menu.html'; ?>
</nav>
<!--- END MENU --->
<div style="margin-top: 60px;"></div>
<!--- BEGIN CONTENT --->
<div id="content" data-ng-controller="MainCtrl">
    <div class="slide slide-animate-container">
        <div class="slide-animate" data-ng-include="site"></div>
    </div>
</div>
<!--- END CONTENT --->
<?php include_once 'web/template/footer.html'; ?>
