<?php
include_once 'config.php';
?>
<!DOCTYPE html>
<html lang="pl" data-ng-app="LoginApp">
<head>
    <title>Zaloguj</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='http://fonts.googleapis.com/css?family=PT+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="web/css/bootstrap.min.css">
    <link rel="stylesheet" href="web/css/loginStyle.css" type="text/css" />
    <!-- Latest compiled and minified CSS -->
    <!-- jQuery library -->
    <script src="web/js/include/jquery-2.1.4.min.js" type="text/javascript"></script>

    <!-- Latest compiled JavaScript -->
    <script src="web/js/include/bootstrap.min.js"></script>
    <script src="web/js/include/angular.min.js"></script>
    <script src="web/js/include/angular-route.min.js"></script>
    <script src="web/js/include/ngDialog.min.js"></script>

    <link rel="stylesheet" href="web/css/ngDialog.css">
    <link rel="stylesheet" href="web/css/ngDialog-theme-default.min.css">
    <link rel="stylesheet" href="web/css/ngDialog-theme-plain.min.css">
    <script src="web/js/LoginApp.js" type="text/javascript"></script>
    <script
        src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit"
        async defer
        ></script>
</head>
<body>
<div class="container" ng-controller="MainCtrl">
    <div class="row">
        <div class="col-sm-5 col-sm-offset-3" >
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>Zaloguj się!</h3>
                </div>
                <div class="panel-body">
                    <form method="post">
                        <div id="alert"></div>
                        <div class="form-group">
                            <label for="login">Login</label>
                            <input type="text" id="login" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Hasło</label>
                            <input type="password" id="password" required>
                        </div>
                        <button class="btn btn-success" data-ng-click="login()"><span class="glyphicon glyphicon-log-in"></span> Zaloguj</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>