var app = angular.module('App', ['ngRoute','ngDialog','ui.bootstrap']);
app.config(function ($locationProvider) {
    $locationProvider.html5Mode(true);
});
app.controller('MainCtrl', function($rootScope,$scope, $http, ngDialog){
    if(!$scope.site) {
        $scope.site = 'web/template/index.html';
    }

    $rootScope.showEdit1 = function () {
        ($rootScope.showEdit) ? $rootScope.showEdit = false : $rootScope.showEdit = true;
    };

    $rootScope.hidePaid1 = function () {
        ($rootScope.hidePaid) ? $rootScope.hidePaid = 0 : $rootScope.hidePaid = 1;
        ($rootScope.hidePaid) ? $rootScope.textHidePaid = "Schowaj" : $rootScope.textHidePaid = "Pokaż";
        var hideId = [], ite = 0;

        if($rootScope.hidePaid === 1) {
            for (var i = 0; i < $rootScope.people.item.length; i++) {
                if($rootScope.people.item[i].hidePaid == 1) {
                    hideId[ite] = i;
                    ite++;
                    $rootScope.people.item[i].hidePaid = 0;
                }
            }

        } else {
            $http.get("inc/get/HidePaid.php").success(function (result) {
                result = atob(result);
                $rootScope.people = angular.fromJson(result);
            });
        }

    };

    $rootScope.openDialog = function(name) {
        ngDialog.open({
            template: 'web/template/' + name + '.html'
        })
    };

    $scope.EditPeriod = function(id,index) {

        $rootScope.periods.item.forEach(function(period){
            if(period.id === id){
                $scope.editPeriod = period;
            }
        });
        $scope.indexOfPeriod = index;
        ngDialog.open({
            template: 'web/template/editPeriod.html',
            scope: $scope
        });
    };

    $scope.edit = function (id,index) {
        var name = $('#name').val(),
            amount = $('#amount').val();

        $.ajax({
            url: 'inc/edit.php',
            type: 'post',
            data: {name: name, amount: amount, id: id}
        }).success(function(data){
            data = atob(data);
            var newData = angular.fromJson(data);

            if(newData.error === false) {
                $scope.$apply(function(){
                    $rootScope.periods.item[index] = newData.data;
                });
                ngDialog.close();
                addAlert('success', 'Poprawnie edytowano "' + newData.data.name + '"');
            } else {
                addAlert('error', newData.error)
            }

        });
    };

    $rootScope.logout = function(){
        $.ajax({
            url: 'inc/logout.php'
        }).success(function(){
             window.location = 'login.php';
        });
    };

    $rootScope.Delete = function(id,index,type){
        $.ajax({
            url: 'inc/delete.php',
            data: {id: id, type: type},
            type: 'post'
        }).success(function(result){
            result = atob(result);
            var data = angular.fromJson(result);

            if(data.error == false){
                $scope.$apply(function(){
                    switch (type) {
                        case 'people':
                            $rootScope.people.item.splice(index,1);
                            break;

                        case 'expense':
                            $rootScope.expenses.splice(index,1);
                            break;

                        case 'periods':
                            $rootScope.periods.item.splice(index,1);
                            break;
                    }
                });
            }
        });
    };

    $rootScope.ForgiveCollect = function () {
        ngDialog.open({
            template: 'web/template/forgive.html',
            scope: $scope
        })
    };

    $rootScope.OpenArchive = function () {
        $http.get('inc/get/getArchive.php').success(function (result) {
            result = atob(result);
            var data = angular.fromJson(result);

            $scope.a_periods = data.periods;
            $scope.a_people = data.people;
            $scope.a_display = data.display;
            $scope.a_paid = data.paid;
        });

        $rootScope.LoadContent('archive');
    };

    $rootScope.LoadContent = function (site) {
        ngDialog.close();
        $scope.site = 'web/template/'+site+'.html';
    };

    $scope.CloseThisYear = function () {
        $http.get('inc/CloseThisYear.php').success(function (data) {
            data = atob(data);
            $rootScope.info = data;
        });
    };

});

app.controller('GetData', function($rootScope,$scope,$http,ngDialog){
    $scope.getDisplay = function (input, number) {
        if(input === null) return false;
        var i=0, len=input.length;

        for (; i<len; i++) {
            if (+input[i] == +number) {
                return true;
            }
        }
        return false;
    };
    
    $http.get('inc/get/fetchMainData.php').success(function(data){
        data = atob(data);
        var newData = angular.fromJson(data);
        $rootScope.people = newData.people;
        $rootScope.periods = newData.periods;
        $rootScope.display = newData.display;
        $rootScope.paid = newData.paid;
        $rootScope.showMessage = 0;
        var countHide = 0;

       for(var i=0;i<$rootScope.people.item.length;i++){
            if($rootScope.people.item[i].hidePaid == 1) {
                countHide+=1;
            }
       }

        if(countHide === $rootScope.people.item.length) {
            $rootScope.showMessage = 1;
        }
    });

    $scope.pay = function(userId, periodId) {
        if(!$rootScope.infoArchive) {
            $.ajax({
                url: 'inc/pay.php',
                type: 'post',
                data: {userId: userId, periodId: periodId}
            }).success(function (data) {
                data = atob(data);
                var newData = angular.fromJson(data);
                
                if (newData.error === false) {
                    var int = toInt(periodId+''+userId),
                        index = $rootScope.paid.indexOf(int[0]);

                    $scope.$apply(function () {
                        if(newData.data == 'add') {
                            $rootScope.paid.push(int[0]);
                            
                            if(newData.archivePeriod) {
                                $scope.text = "Zapłacono wszystkie składki w tym okresie. Czy chesz przenieść okres do archiwum? UWAGA!! Po tej operacji nie będzie można edytować płatności!";
                                $scope.id = periodId;
                                ngDialog.open({
                                    template: "web/template/confirmTemplate.html",
                                    scope: $scope
                                });
                            }
                        } else {
                            $rootScope.paid.splice(index, 1);
                        }
                    });
                }
            })
        } else {
            alert('Nie można edytować sładek w archiwum!');
        }
    };

    $scope.ok = function (id) {
        $http.get('inc/ArchivedPeriod.php?id='+id).success(function (result) {
            result = atob(result);
            var result1 = angular.fromJson(result);
             if(!result1.error) {
                 for(var i=0;$rootScope.periods.item.length>i;i++) {
                     if($rootScope.periods.item[i].id == id) {
                         $rootScope.periods.item.splice(i,1);
                         break;
                     }
                 }
             } else {
                 alert('Błąd! Skontaktuj sie z administratorem! '+result1.error);
             }
            
            ngDialog.close();
        });
    };

    $scope.cancel = function () {
        ngDialog.close();
    };

    $scope.toInt = function(number) {
        return number.map(Number);
    };

    $scope.Exclude = function(id,index) {
        $.ajax({
            url: 'inc/exclude.php',
            data: {id:id},
            type: 'post'
        }).success(function(data) {
            data = atob(data);
            var newDate = angular.fromJson(data);
            if(newDate.error === false) {
                $scope.$apply(function(){
                    $rootScope.people.item.splice(index,1);
                });
            }
        });
    };

    $scope.checkPaid = function (user, period) {
        var int = toInt(period+''+user),
            i = $rootScope.paid.indexOf(int[0]);

        return (i === -1) ? false : true;
    }
});

app.controller('AddItem', function($rootScope,$scope,ngDialog,$window) {
    $scope.addCollect = function(){
        var name = $('#name').val(),
            amount = $('#amount').val();

        $.ajax({
            url: 'inc/add/addCollect.php',
            type: 'post',
            data: {name:name, amount:amount}
        }).success(function(data){
            data = atob(data);
            var newData = angular.fromJson(data);
            if(newData.error === false) {
                $scope.$apply(function(){

                    if(newData.archive == 1) {
                        $rootScope.periods.item.shift();
                    }

                    $rootScope.periods.item.push(newData.newData[0]);
                    for(var i=0;$rootScope.people.item.length > i;i++){
                        $rootScope.people.item[i].hidePaid = 0;
                    }

                });
                $rootScope.showMessage = 0;
                addAlert('success', 'Poprawnie dodano składkę!');
                ngDialog.close();
            } else {
                addAlert('error', newData.error);
            }
        });
    };

    $scope.addExpense = function(){
        var name = $('#name').val(),
            price = $('#price').val(),
            date = $('#date').val();

        $.ajax({
            url: 'inc/add/addExpense.php',
            type: 'post',
            data: {name: name, price: price, date: date}
        }).success(function(data){
            data = atob(data);
            var newData = angular.fromJson(data);

            if(newData.error === false) {
                addAlert('success','Poprawnie dodano wydatek!');
                $scope.$apply(function () {
                    $rootScope.LoadContent('expenses');
                });
            } else {
                addAlert('error', newData.error);
            }
        });
    };

    $scope.addPayment = function(){
        var name = $('#name').val(),
            price = $('#price').val(),
            date = $('#date').val();

        $.ajax({
            url: 'inc/add/addPayment.php',
            type: 'post',
            data: {name: name, price: price, date: date}
        }).success(function(data){
            data = atob(data);
            var newData = angular.fromJson(data);

            if(newData.error === false) {
                addAlert('success','Poprawnie dodano wpłatę!');
                $scope.$apply(function(){
                    $rootScope.LoadContent('revenue');
                });
            } else {
                addAlert('error', newData.error);
            }
        });
    };

    $scope.addPerson = function(){
        var name = $('#name').val();

        $.ajax({
            url: 'inc/add/addPerson.php',
            data: {name: name},
            type: 'post'
        }).success(function(data){
            data = atob(data);
            var newData = angular.fromJson(data);
            if(newData.error === false){
                $scope.$apply(function () {
                    $rootScope.people.item.push(newData.person[0]);
                });

                $scope.$apply(function () {
                    $rootScope.display = $rootScope.display.concat(newData.display);
                });
                addAlert('success','Poprawnie dodano dłużnika!');
                ngDialog.close();
                $window.location.reload();
            } else {
                addAlert('error', newData.error);
            }

        });
    };
});

app.controller('Balance', function($rootScope,$scope,$http,ngDialog){
    $http.get('inc/Balance.php').success(function(data){
        data = atob(data);
        var newData = angular.fromJson(data);
        $rootScope.overdues = newData.overdue;
        $scope.revenue = newData.revenue;
        $scope.availableBudget = newData.availableBudget;
        $scope.totalShortage = newData.totalShortage;
        $scope.totalExpenses = newData.totalExpenses;
        $scope.planExpenses = newData.planExpenses;
        ToPaginate(newData.expenses.item.reverse());
        $scope.payments = newData.payments;
        $scope.payCollect = newData.payCollect;
    });

    $scope.EditExpense = function(id) {
        $scope.expense = $scope.expenses[id];
        ngDialog.open({
            template: 'web/template/editExpense.html',
            scope: $scope
        });
    };

    $scope.UnpaidCollect = function(id) {
        $scope.forgive = true;
        $http.get('inc/get/UnpaidCollect.php?id='+id).success(function (result) {
            result = atob(result);
            $scope.periods1 = angular.fromJson(result);
            $scope.forgiveId = id;
        })
    };

    $scope.ForgiveCollect = function(periodId, personId) {
        $.ajax({
            url:'inc/forgiveCollect.php',
            data: {periodId: periodId, personId: personId},
            type: 'post'
        }).success(function(data){
            data = atob(data);
            var newData = angular.fromJson(data);

            if(newData.error == false) {
                $scope.$apply(function () {
                    $rootScope.display.push(personId+''+periodId);
                });
            }
            ngDialog.close();
        });
    };

    $scope.AcceptEditExpense = function(){
        var name = $('#name').val(),
            price = $('#price').val(),
            date = $('#date').val(),
            id = $('#id').val();

        $.ajax({
            url: 'inc/editExpense.php',
            data: {name:name,price:price,date:date,id:id},
            type: 'post'
        }).success(function(data){
            data = atob(data);
            var newData = angular.fromJson(data);

            if(newData.error === false){
                $scope.$apply(function () {
                    var i = 0;
                   $scope.expenses.forEach(function(expense){
                        if(expense.id == id) {
                            $scope.expenses[i].name  = name;
                            $scope.expenses[i].price = price;
                            $scope.expenses[i].date  = date;
                        }
                       i++;
                   });
                    ngDialog.close();
                });

            } else {
                addAlert('error', newData.error);
            }
        });
    };

    $scope.PayPlanExpense = function (index,id) {
        $.ajax({
            url: 'inc/PayPlanExpense.php',
            data: {id:id},
            type: 'post'
        }).success(function (data) {
            data = atob(data);
            var newData = angular.fromJson(data);

            if(newData.error === false){
                $scope.$apply(function () {
                    $scope.expenses = newData.data;
                });
            }
        })
    };

    function ToPaginate (data) {
        $scope.filtredTodos = [];
        $scope.currentPage = 1;
        $scope.numPerPage = 10;
        $scope.maxSize = 4;

        $scope.numPages = function () {
            return Math.ceil(data.length / $scope.numPerPage);
        };

        $scope.$watch('currentPage + numPerPage', function () {
            var begin = (($scope.currentPage - 1) * $scope.numPerPage),
                end = begin+$scope.numPerPage;

            $rootScope.expenses = data.slice(begin,end);
        });

    }
});

app.controller('GetArchive', function ($scope, $http) {
    $scope.checkPaid = function (user, period) {
        var int = toInt(period+''+user),
            i = $scope.paid.indexOf(int[0]);

        return (i === -1) ? false : true;
    };

    $scope.getDisplay = function (input, number) {
        if(input === null) return false;
        var len=input.length;

        for (var i=0; i<len; i++) {
            if (+input[i] == +number) {
                return true;
            }
        }
        return false;
    };
});

app.filter('inArray',function(){
    return function(array,value) {
        return array.indexOf(value) !== -1;
    }
});

app.directive('ngConfirmClick', [
    function(){
        return {
            link: function ($scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                        $scope.$eval(clickAction)
                    }
                });
            }
        };
    }]);

app.directive('content', function () {
    return {
        scope: true,
        templateUrl: function(elem,attr){
            return 'web/template/' + attr.site + '.html';
        }
    }
});

function addAlert(type, message) {
    if(type == 'error') {
        return $('#alert').html('<div class="alert alert-danger"><span class="glyphicon glyphicon-remove-circle"></span> Błąd! '+ message +'</div>');
    } else if(type == 'success') {
        return $('#alert').html('<div class="alert alert-success"><span class="glyphicon glyphicon-ok-circle"></span> ' + message + '</div>');
    }
}

window.setInterval(function(){
    $.ajax({
        url: 'inc/CheckSession.php'
    }).success(function(data){
        if(data === 'false') {
            window.location = 'login.php';
        }
    });
},5000);

function toInt (number) {
    return number.split(',').map(Number);
}

app.directive('confirm', function(ConfirmService) {
    return {
        restrict: 'A',
        scope: {
            eventHandler: '&ngClick'
        },
        link: function(scope, element, attrs){
                element.unbind("click");
                element.bind("click", function(e) {
                    ConfirmService.open(attrs.confirm, scope.eventHandler);
                });
        }
    }
});

app.service('ConfirmService', function($modal) {
    var service = {};
    service.open = function (text, onOk) {
        var modalInstance = $modal.open({
            templateUrl: 'web/template/confirmTemplate.html',
            controller: 'ModalConfirmCtrl',
            resolve: {
                text: function () {
                    return text;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            onOk();
        }, function () {
        });
    };

    return service;
});

app.controller('ModalConfirmCtrl', function ($scope, $modalInstance, text) {

    $scope.text = text;

    $scope.ok = function () {
        $modalInstance.close(true);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});