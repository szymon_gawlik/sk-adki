var app = angular.module('LoginApp',['ngRoute']);

app.controller('MainCtrl', function($scope){
    $scope.login = function(){
        var login = $("#login").val(),
            password = $("#password").val();

        $.ajax({
            url: 'inc/login.php',
            type: 'post',
            data: {login:login, password: password}
        }).success(function(data){
            console.log(data);
            var newData = angular.fromJson(data);
            if(newData.error == false) {
                window.location = 'index.php';
            } else {
                $('#alert').html('<div class="alert alert-danger"><strong><span class="glyphicon glyphicon-remove"></span> '+newData.error+'</strong></div>');
            }
        })
    }
});